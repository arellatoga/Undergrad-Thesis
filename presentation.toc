\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Background of the Study}{4}{0}{1}
\beamer@sectionintoc {2}{Theoretical Framework}{8}{0}{2}
\beamer@sectionintoc {3}{Related Work}{11}{0}{3}
\beamer@sectionintoc {4}{Statement of the Problem}{34}{0}{4}
\beamer@sectionintoc {5}{Significance of the Study}{37}{0}{5}
\beamer@sectionintoc {6}{Objectives of the Study}{40}{0}{6}
\beamer@sectionintoc {7}{Scope and Limitations of the Study}{43}{0}{7}
\beamer@sectionintoc {8}{Research Plan}{45}{0}{8}
\beamer@sectionintoc {9}{Bibliography}{46}{0}{9}

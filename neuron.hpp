#pragma once
#include <vector>
#include <string>

class rule;

class neuron {
    public:
    neuron (std::string _name, int _spikes){
        name = _name;
        spikes = _spikes;
    }

    std::string name;

    private:
    int spikes;
    //std::vector<neuron> synapses;
    std::vector<rule> rules;
};

\documentclass[a4paper, 12pt]{article}
%\usepackage{times}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{amsfonts}
\usetikzlibrary{calc}
\usepackage[english]{babel}

\topmargin      0.0in
\headheight     0.0in
\headsep        0.0in
\oddsidemargin  0.0in
\evensidemargin 0.0in
\textheight     9.0in
\textwidth      6.5in

\title{{\bf Creation of a Simulator for Spiking Neural P Systems with
    Neural Division and Dissolution}}
\author{{\bf Arel Latoga} \\
             2013-14806
}

\begin{document}
\pagestyle{plain}
\pagenumbering{roman}
\maketitle
\thispagestyle{empty}
\pagebreak

\begin{abstract}
    Among spiking neural network models, Spiking Neural P systems are the ones that have recently gained
    a lot of traction due to its being relatively younger and because of its flexibility in being modified
    to create newer models. Though able to compute fast for NP-hard problems,
    simulation is still long and may take up to hours due to its non-determinism and neuron count.
    Various efforts have already been attempted to simulate smaller Spiking Neural
    P systems but none as of yet can simulate Spiking Neural P systems with Division and Dissolution.
    Due to the volatile neuron nature of these systems, simulation by paper will prove to be difficulty,
    especially for problems of increasing input sizes. As such, this proposal aims to provide an automated alternative
    to paper simulation for systems of smaller scale.
\end{abstract}

\pagebreak

\tableofcontents

\cleardoublepage
\pagenumbering{arabic}
\section{Background of the Study}
    %
    Spiking Neural P Systems (SNP systems for short) are theoretical models patterned after
    concepts in neuroscience.
    %
    The model consists of neurons connected to each other via synapses.
    %
    These neurons fire spikes (similar to brain neurons sending electric impulses)
    to connected neurons when triggered.
    %
    The firing of these spikes may be delayed.
    %
    Various solutions have already been implented to simulate SNPs but they are 
    restricted by several factors, such as non-determinism or presence of delay in the rules.
    %
    An implementation of SNPs in CUDA with division and dissolution may help us speed up
    the understanding of the capabilities of this theoretical model.

\section{Theoretical Framework}
    The very nature of membrane computing takes inspiration from how biological bodies work, from cells, organs, and so on \cite{memcomp}.
    These cells, called \textit{neural cells}, transmit electrical neurons or \textit{spikes} or \textit{action potential} to other neural cells \cite{snp}.
    SNPs similarly use spiking neurons, which transmit their own spikes in the form of regular expressions.
    As defined by Sipser \cite{sipser}, let the definiion of regular expressions be:\\
    \textbf{Definition: Regular Expressions}\\
    Say that \textit{R} is a \textit{\textbf{Regular Expression}} if \textit{R} is 
    \begin{enumerate}
        \item
            \textit{a} for some \textit{a} is in the alphabet $\Sigma$,
        \item
            $\epsilon$,
        \item
            $\emptyset$,
        \item
            $(R_1 \cup R)2$, where $R_1$ and $R_2$ are regular expressions,
        \item
            $(R_1 \circ R)2$, where $R_1$ and $R_2$ are regular expressions, or
        \item
            $(R_1^*)$, where $R_1$ is a regular expression.
    \end{enumerate}
    In items 1 and 2, the regular expressions $a$ and the $\epsilon$ represent the languages
    $\{a\}$ and $\{\epsilon\}$, respectively. In item 3, the regular expression $\emptyset$
    represents the empty language. In items 4, 5, and 6, the expressions represent the languages 
    obtained by taking the union of concatenation of the languages $R_1$ and $R_2$, or the star
    of the language $R_1$, respectively.\\
    \textbf{Definition: SNP system} \\
    An SNP system of a finite degree $m \leq 1$, according to \cite{snp-tutorial},
    is a construct of the form  
    $\Pi = (O, \sigma_1,...,\sigma_m, syn, in, out)$, where :
    \begin{enumerate}
        \item
            $ O = \{a\}$ is the singleton alphabet of spike $a$.
        \item
            $\sigma_1,...,\sigma_m$ are \textit{neurons}, taking the form of
            $\sigma_i =(n_i, R_i), 1 \leq i \leq m$, where:
            \begin{enumerate}
                \item 
                    $n_i \geq 0$ is the initial number of spikes contained in $\sigma_i$;
                \item
                    $R_i$ is a finite set of rules of the following two forms:
                    \begin{itemize}
                        \item
                            $E/a^c \rightarrow a^P;d$, where $E$ is a regular expression over
                            $a$ and $c \geq p \geq 1, d \geq 0$
                        \item 
                            $a^s \rightarrow \lambda$, for $s \geq 1$, with the restriction that 
                            for each rule $E/a^c \rightarrow a^P;d$ of type (1) from $R_i$ we have
                            $a^s \not\in L(E)$
                    \end{itemize}
            \end{enumerate}
        \item
            $syn \subseteq \{1,2,...,m\}\times\{1,2,...,m\}$ with $i /neq j$ for all $(i,j) \in syn, 1 \leq i, j \leq m$ (synapses between neurons)
        \item
            $in, out \in \{1, 2, ..., m\}$ indicate the input and the output neurons, respectively
    \end{enumerate}
    Rules classified under type (1) are firing rules and rules classified under type (2) are 
    forgetting rules.
    When a firing rule $E/a^c \rightarrow a^P;d$ is applied, $c$ amount of spikes are consumed
    and $P$ amount of spikes are sent to other neurons connected by synapses after a time delay $d$.
    Whenever a neuron is delaying its firing, it is closed
    i.e. spikes are not allowed to enter the neuron and will be discarded.
    If $d = 0$, spikes are fired immediately. If $d = 1$, spikes will be fired after 1 time step, and so on.
    When a forgetting rule $a^s \rightarrow \lambda$ is applied, a neuron will lose all of its spikes.\\
    Non-determinism occurs when two different rules can be applied by the same number of spikes e.g.
    rules $E/a^c \rightarrow a^P_1;d$ and $E/a^c \rightarrow a^P_2;d$, for whatever
    values $P_1$ and $P_2$ are, both exist in the same neuron. \\
    When a spike leaves via the output neuron, the spike trains and the timestep may be considered to be
    the output neuron, though it may vary among SNP systems. The outputs may not be the same each time,
    since non-determinism can change the configuration.

\section{Related Work}
    \subsection{Definitions}
    According to \cite{oxford}, SNPs heavily borrow concepts from the human body. 
    Like in the brain, SNP systems have \textit{spikes} that are emitted from \textit{neurons}.
    Spikes may enter via \textit{spike trains} and leave the system to give the output.\\
    %
    Efforts to represent SNPs in a mathematical manner have already been done before.
    A working simulator for non-deterministic SNPs with no delay \cite{gpusim} has already been attempted before: \\
    %
    \textbf{Definition: Configuration Vectors} \\
    A \textit{configuration vector} $C_k = \langle \alpha_1,...,\alpha_2 \rangle,\alpha_j \in \mathbb{N}$
    is a vector representing the number of spikes present in each neuron pertaining to the $kth$ time step. \\
    %
    \textbf{Definition: Spiking Vectors} \\
    A \textit{spiking vector} $S_k = <S_{k_(1)},...,S_{k_(n)} >$ corresponds to a $C_k$ such that
    \begin{enumerate}
        \item
            for each $i$ $(1\leq i \leq n), S_{k_(i)} = 1$ whenever rule $r_i$ is applicable to 
            $C_k$, otherwise $S_{k_(i)} = 0$
        \item
            if $\{i_1,...,i_t\}$ verifies $S_{k_(i)} = 1 (1 \leq j \leq t)$ and $S_{k_(i)} = 0$
            for $p \not\in \{i_1,...,i_t\}$, then rules $r_{i1},...,r_{it}$ are simultaneously applicable
            to $C_k$
    \end{enumerate} 
    \noindent
    % 
    \textbf{Definition: Spiking Transition Matrix}\\
    %
    The \textit{spiking transition matrix} $M_\Pi$ is an $n \times m$ matrix consisting of elements
    $a_{ij}$ given as \\
    \begin{equation}
        a_{ij} =
        \begin{cases}
            -c & \text{rule $r_i$ is in $\sigma_j$ and consumes $c$ spikes}\\
            p & \text{rule $r_i$ is in $\sigma_s ((s,j)\in syn)$ producing $p$ spikes in total}\\
            0 & \text{rule $r_i$ is in $\sigma_s ((s,j)\in syn)$}

        \end{cases}
    \end{equation}
    %
    And finally, \\
    %
    \textbf{Equation 1: $C_{k+1}$}
    $$ C_{k+1} = C_k + S_k \cdot M_\Pi$$
    %
    As seen here, simple matrix representation of the number of spikes present per timestep $C_k$ and 
    the possible spiking options $S_k$ can easily compute for the next possible configuration vector.
    %
    But this only applies to SNPs with rules with no delay.\\
    % 
    Carandang et. al \cite{cusnp} have also proven that deterministic SNPs with delay can be
    expressed using matrices and thus allow computer simulations.
    %
    In addition to the defintions above, there are more:\\
    %
    \textbf{Definition: Status Vector}\\
    The vector $St_k = <st_1,...,st_m>$ is the status vector at the $kth$ step for each neuron represented
    by $st_i$ where\\
    \begin{equation}
        st_i =
        \begin{cases}
            1 & \text{if neuron $m$ is open}\\
            0 & \text{if neuron $m$ is closed}
        \end{cases}
    \end{equation}
    %
    \textbf{Definition: Delay Vector}\\
    The delay vector $D = <d_1,...,d_m>$ stores the amount of delay for each rule $r_i, i = 1,...,n$ in $\Pi$ \\
    %
    \textbf{Definition: Loss Vector}\\
    The loss vector $LV_k = <lv_1,...,lv_m>$ is the vector where for $lv_i$ for each neuron $\sigma_i, i =1,...,m$
    contains the number of spikes consumed of the rule in the specific neuron $\sigma_i$ at the $kth$ step.\\
    %
    \textbf{Definition: Gain Vector} \\
    The gain vector $GV_k = <gv_1,...,gv_m>$ is the gain vector which contains the total number of spikes gained,
    $gv_i$ for each neuron $\sigma_i, i =1,...,m$ at the $kth$ step, regardless if it is open or closed. \\
    %
    \textbf{Definition: Transition Vector} \\
    Let $d:1,...,n$ be a total order given for all the $n$ rules, the transition vectors of $\Pi$,
    is a set of vectors $Tv$ defined as $Tv = \{tv_1,....,tv_n\}$ where each $tv_i=<p_1,...,p_m>$ and\\
    \begin{equation}
        p_j =
        \begin{cases}
            p & \text{if rule $r_i$ is in $\sigma_s (s \neq j$ and $ (s \neq j) \in syn)$ and it is applied producing $p$ spikes}\\
            0 & \text{if rule $r_i$ is in neuron $\sigma_s (s \neq j$ and $ (s \neq j) \in syn)$}
        \end{cases}
    \end{equation}
    %
    \textbf{Definition: Indicator Vector}\\
    The indicator vector $IV_k = <iv_1,...,iv_m>$ will be mupltiplied to the transition vector
    to obtain the net number of spikes a neuron receives, regardless if it's open or closed.\\
    %
    \textbf{Definition: Net Gain Vector} \\
    The net gain vector $NG_k = GV_k \otimes st_k - LV_k$ at step $k$ where $\otimes$ is the element-wise
    multiplication operator. \\
    Finally, we have the equation to compute for the configuration vectors themselves. \\
    \textbf{Equation 2: $C_{k+1}$ for SNPs with delay}\\
    $$ C_{k+1} = C_k + NG_k $$
    %
    The algorithm described for simple SNPs \cite{cusnp} are much more complex compared to the one designed for SNPs with delay.
    For SNPs with no delay, the following equation is used \cite{gpusim}:\\
    \textbf{Equation 3: $C_{k+1}$ for SNPs with no delay}\\
    $$ C_{k+1} = C_k + S_k \cdot M_\Pi$$
    The computation is straightforward, and does not require any other matrices.\\
    As such, the main focus of this problem is to present SNPDDs formulated by \cite{snpdd} and \cite{cusnp}.
    in a similar fashion to allow automation. \\
    %
    Another form of the SNP system, the SNP system with Division and Budding (SNPBD system), also exists.
    Details regarding this SNP will not be in-depth, other than the fact that it is similar to
    SNPDDs except that neurons can dissolve into two and can also produce child neurons without destroying itself. \\
    As SNPs have already been defined above, Spiking Neural P System with Neuron Division and Dissolution
    (shortened to SNPDD system from now on). Let the definition of the SNPDD system be below: \\
    \textbf{Definition: SNPDD system} \\
    An SNPDD system of a finite degree $m \leq 1$
    is a construct of the form\\
    $\Pi = (O, H, syn, n_1, n_2,...,n_m,R,in,out)$, where:
    \begin{enumerate}
        \item
            $O = \{s\}$ is the \textit{singleton alphabet} where $s$ is a spike.
        \item
            $H$ is the set of \textit{labels} for the neurons.
        \item
            $syn \subseteq H \times H$ represents a \textit{dictionary of the synapses} for each
            and every neuron ($1 \leq i \leq m, (i,i) \notin syn$)
        \item
            $n_i \geq 0$ represents the \textit{initial number of spikes} in neuron $\sigma_i$ for
            $1 \leq i \leq m$.
        \item
            $R$ represents the set of \textit{all developmental rules}, taking either of the four forms:
            \begin{itemize}
                \item firing rule
                \item forgetting rule
                \item neuron division rule
                \item neuron dissolution rule
            \end{itemize}
        \item
            $in, out \subseteq H$ indicate the \textit{input and output} neurons of $\Pi$, respectively.
    \end{enumerate}
    \noindent
    At timestep $t = 0$, the synapse dictionary $syn$ shows the starting structure of the SNPDD system.
    From here, the neurons may start to interact by firing spikes. 
    If a specific neuron $\sigma_i$ possesses $h$ amount of spikes, a firing rule $[E/a^c \rightarrow a^P;d]$ may be applied by consuming $c$ amount of spikes.
    When a rule is applied, $P$ amount of spikes will be discharged to neighbor neurons after a time delay $d$.
    The quantity of $d$ will decrease by 1 with each timestep and when it finally equals to 0, the spikes will be released.\\
    Another possibility is for a forgetting rule $[E/a^c \rightarrow \lambda]$ to be applied. 
    A neuron may lose, or "forget", the number of spikes it has when this rule is applied.\\
    Neuron division $[E]_i \rightarrow []_j || []_k$ may be applied, too. 
    All child neurons will inherit the parent's synapses, but not the spikes.
    The child neurons may also gain new synapses as dictated by $syn$.
    Likewise, $syn$ may be updated due to the child neurons' inheritance of synapses.
    Neurons that allow for neuron division may not have synapses to itself.\\
    Neuron dissolution $[E]_i \rightarrow \sigma_i$ is also legal, where the neuron may entirely remove itself.
    All its synapses are removed, too.\\
    %
    \subsection{Hardware aspects}
    %
    NVIDIA's Compute Unified Device Architecture (or CUDA) enable for fast simulations, as in the case of the 
    simulator for SNPs with delays \cite{cusnp}. \textit{Kernel functions} are utilized by threads in \textit{thread blocks}.
    These blocks compute side-by-side, allowing large volumes of data to be processed. \\
    For the SNP with no delay simulator, the total amount of device global memory $G$ needed can be
    expressed through the following formula \cite{gpusim}:
    $$ G \geq q_k (n + 2m) + m (n + 1) $$ 
    where $q_k$ is the number of \textit{configuration vectors},  $m$ is the number of neurons and
    $n$ is the number of rules.\\
    The amount of $G$ must be taken into account because some problems can not be simulated under certain
    hardware and thus saving time to see if it will successfully run. Although the equation 
    can't be applied to SNPDDs, an equivalent equation may be formulated to serve the same purpose.
    This may take into account the number of rows present in each matrix, considering all the matrices described for
    the simulator with delays \cite{cusnp}.\\
    %
    \subsection{Software aspects}
    NP-Complete systems may be solved via regular SNP systems \cite{oxford}. These NP-Complete
    systems are \textit{decision problems}, problems that return only two possible answers: 
    \textit{yes} or \textit{no}. Various problems that have been solved are the \textit{SAT problem}
    and the \textit{Subset Sum Problem}.\\
    Since SNPDD systems are more capable than ordinary SNP systems, problems can be solved with a lesser
    amount of time. For the \textit{SAT problem} of inputs $n$ boolean variables and $m$ clauses,
    the SNPDD system can solve it in $2n + m + 3$ steps while other SNP systems like the SNPDB
    and \textit{SNP systems with neuron division} 
    have to do it a significantly larger amount of time \cite{snpdd}.

\section{Statement of the Problem}
    SNPDD systems are very powerful models that are able to solve NP-hard problems faster than other SNP systems \cite{snpdd}.
    %
    However, due to their nature, they tend to be cumbersome to simulate by standard pen-and-paper techniques
    and thus automation is needed. Therefore, this research aims to answer the following questions:
    \begin{enumerate}
    \item
        Can SNPDD systems be represented into vectors and matrices?
    \item
        Can an algorithm be formulated using the representation?
    \item
        Can a computer simulate it through existing technologies (i.e. CUDA)?
    \end{enumerate}

\section{Significance of the Study}
    SNPDDs already pave way for solving problems classified under NP-hard. 
    %
    Creation of a simulator for these problems would make solving easier, and may allow researchers
    to save time by reducing the time and effort spent on solving when compared to pen-and-paper techniques.
    %
    Additionally, further models may be developed later on that may take inspiration from this.

\section{Objectives of the Study}
    In line with the statements of the problem, the goals of this research are:
    \begin{enumerate}
        \item
            Represent the SNPDD system into vectors and matrices,
        \item
            Formulate an algorithm that allows computation of SNPDD systems,
        \item
            Create a proof-of-concept simulator that uses the algorithm for SNPDD systems.
    \end{enumerate}

\section{Scope and Limitations of the Study}
    %A working algorithm must accept any kind of SNPDD as input, with the SNPDD tuple converted into their own vectors and matrices.
    The working algorithm must be able to simulate the simplest of SNPDD systems -
    such as the representations of the \textit{SAT problem} or the \textit{Subset Sum problem}.
    %
    Though, due to hardware limitations such as limited memory in the GPU, SNPDD systems of larger scale may fail to simulate.
    %
    Other forms of SNP systems exist, like SNP systems with Division and Budding, Small Universal SNP systems with Structural Plasticity,
    or SNP systems with weights. 
    %
    These forms will not be tackled by this special problem but results of this research may somehow
    prove to be helpful for those.
    %
    Examples of SNPDD systems, in the forms of the SAT problem and the Subset Sum problem \cite{snpdd}, were deterministic, 
    therefore the simulator must be able to simulate deterministic problems.

\section{Research Plan}
    \subsection{Methodology}
    To succeed in solving this special problem, several steps are to be done.
    \begin{enumerate}
        \item
            Further review of literature related to the topic at hand.
        \item
            Study the nature of SNPDD systems and possible patterns that may arise.
        \item
            Formulate an algorithm for computing SNPDD systems.
        \item
            Use NVIDIA's CUDA technology to create a simulator.
        \item
            Document the entire process.
    \end{enumerate}

    \subsection{Timetable}
   % 
    Figure 1 below shows the Gantt chart for the progression of the research. 
   % 
    For Phase 1, research will be done on work related to the topic at hand.  
   % 
    Phase 2 will be when the formulation of a working algorithm is done.
   % 
    In Phase 3, a proper simulator will be programmed.
    %
    The entirety of Phase 4, which overlaps upon phases, will be when the paper will be made
    while testing for benchmarks on the simulator when ready.
    %
    Comparisons will be made against existing simulators when applicable, and if not, will just state its feasibility.

    \subsection{Research Outputs}
    After the allotted time for the research, the following are to be expected:
    \begin{itemize}
        \item
            A paper detailing the intricate process of the algorithm and an analysis of its complexity.
        \item
            Source code for an SNPDD system simulator coded on NVIDIA's CUDA, available for compilation and running.
        \item
            Analyses of given inputs, the SAT problem and the Subset Sum, and benchmarking of performance based on increasing input size.
    \end{itemize}

\newpage

    \def\GanttHeader#1#2#3#4{%
    \pgfmathparse{(#1-#2-#3)/#4}
    \tikzset{y=7mm, task number/.style={left, font=\bfseries},
     task description/.style={text width=#3,  right, draw=none,
           font=\sffamily, xshift=#2,
           minimum height=2em},
     gantt bar/.style={draw=black, fill=blue!30},
     help lines/.style={draw=black!30, dashed},
     x=\pgfmathresult pt
     }
    \def\totalmonths{#4}
    \node (Header) [task description] at (0,0) {\textbf{Phases}};
    \begin{scope}[shift=($(Header.south east)$)]
    \foreach \x in {1,...,#4}
      \node[above] at (\x,0) {\footnotesize\x};
    \end{scope}
    }

    \def\Task#1#2#3#4{%
    \node[task number] at ($(Header.west) + (0, -#1)$) {#1};
    \node[task description] at (0,-#1) {#2};
    \begin{scope}[shift=($(Header.south east)$)]
      \draw (0,-#1) rectangle +(\totalmonths, 1);
      \foreach \x in {1,...,\totalmonths}
        \draw[help lines] (\x,-#1) -- +(0,1);
      \filldraw[gantt bar] ($(#3, -#1+0.2)$) rectangle +(#4,0.6);
    \end{scope}
    }

    \begin{figure}
    \thispagestyle{empty}
    \begin{tikzpicture}
    \GanttHeader{.8\textwidth}{2ex}{2cm}{10}
    \Task{1}{$ \sim $5 mos.}{0}{5}
    \Task{2}{$ \sim $3 mos.}{4}{3}
    \Task{3}{$ \sim $4 mos.}{6}{4}
    \Task{4}{$ \sim $6 mos.}{4}{6}
    %  \Task{7}{Prepare final report}{11}{1}
    \end{tikzpicture}
    \caption{Month 1 (first bar segment) is August 2016} 
    \label{gantt-fig}
    \end{figure}

\clearpage
\nocite{*}
\bibliography{refs}
\bibliographystyle{apalike}

\end{document}

\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage[english]{babel}
\graphicspath{ {slides_pics/}}
\usetheme{boxes}
\usecolortheme{beaver}
\usetikzlibrary{calc}

\title{Creation of a Simulator for Spiking Neural P Systems
    with Neuron Division and Dissolution}
\author{Gerard Arel H. Latoga}
\date{November 29, 2016}

\begin {document}

\frame{\titlepage}

\begin{frame}
\frametitle{Abstract}
    Among spiking neural network models, Spiking Neural P systems are the ones that have recently gained
    a lot of traction due to its being relatively younger and because of its flexibility in being modified
    to create newer models. Though able to compute fast for NP-hard problems,
    simulation is still long and may take up to hours due to its non-determinism and neuron count.
    Various efforts have already been attempted to simulate smaller Spiking Neural
    P systems but none as of yet can simulate Spiking Neural P systems with Division and Dissolution.
    Due to the volatile neuron nature of these systems, simulation by paper will prove to be difficulty,
    especially for problems of increasing input sizes. As such, this proposal aims to provide an automated alternative
    to paper simulation.
\end{frame}

\begin{frame}[shrink=10]
    \frametitle{Table of Contents}
    \tableofcontents
\end{frame}

%%%%%
\section{Background of the Study}
\begin{frame}
\frametitle{Background of the Study}
\begin{itemize}
    \item<1->
        What are Spiking Neural P systems (SNP systems)?
    \item<2->
        How is a simulator going to be implemented?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Theoretical Framework}
\begin{itemize}
    \item<1->
        SNPs are inspired by concepts in biology.
    \item<2->
        SNPs are built upon \textbf{regular languages}.
\end{itemize}
\end{frame}

%%%%%
\section{Theoretical Framework}
\begin{frame}
\frametitle{Theoretical Framework}
\begin{alertblock}{Definition: Regular Languages}
    A language is called a \textit{regular language} if some 
    finite automaton recognizes it.
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Theoretical Framework}
\begin{itemize}
    \item<1->
        SNPs are inspired by concepts in biology.
    \item
        SNPs are built upon \textbf{regular languages}.
    \item
        SNPs have functions similar to finite automata.
\end{itemize}
\end{frame}

\begin{frame}[shrink=5]
\frametitle{Theoretical Framework}
\centering
\begin{alertblock}{Definition: Spiking Neural P Systems} 
    An SNP system of a finite degree $m \leq 1$, according to \cite{snp-tutorial},
    is a construct o the form \\
    $\Pi = (O, \sigma_1,...,\sigma_m, syn, in, out)$, where :
    \begin{enumerate}
        \item
            $ O = \{a\}$ is the singleton alphabet of spike $a$.
        \item
            $\sigma_1,...,\sigma_m$ are \textit{neurons}, taking the form of
            $\sigma_i =(n_i, R_i), 1 \leq i \leq m$, where:
            \begin{enumerate}
                \item 
                    $n_i \geq 0$ is the initial number of spikes contained in $\sigma_i$;
                \item
                    $R_i$ is a finite set of rules of the following two forms:
                    \begin{itemize}
                        \item
                            $E/a^c \rightarrow a^P;d$, where $E$ is a regular expression over
                            $a$ and $c \geq p \geq 1, d \geq 0$
                        \item 
                            $a^s \rightarrow \lambda$, for $s \geq 1$, with the restriction that 
                            for each rule $E/a^c \rightarrow a^P;d$ of type (1) from $R_i$ we have
                            $a^s \not\in L(E)$
                    \end{itemize}
            \end{enumerate}
        \item
            $syn \subseteq \{1,2,...,m\}\times\{1,2,...,m\}$ with $i /neq j$ for all $(i,j) \in syn, 1 \leq i, j \leq m$ (synapses between neurons)
        \item
            $in, out \in \{1, 2, ..., m\}$ indicate the input and the output neurons, respectively
    \end{enumerate}
\end{alertblock}
\end{frame}

%%%%%
\section{Related Work}
\begin{frame}
\frametitle{Related Work}
\begin{itemize}
    \item<1->
        Concept of spiking comes from the brain.
    \item<2->
        Spike trains enable input and output.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Related Work}
\begin{itemize}
    \item<1->
        Efforts to create a simulator are not new.
    \item<2->
        A simulator for SNP systems with non-determinism and no delay exists.
    \item<3->
        A simulator for deterministic SNP systems with delay exists.
\end{itemize}
\end{frame}

\begin{frame}[shrink=10]
\frametitle{Related Work}
\centering
\begin{alertblock}{Matrices and vectors used in simulating SNP systems}
   \begin{itemize}
        \item Configuration Vectors
        \item Spiking Vectors
        \item Spiking Transition Matrix
        \item Status Vectors
        \item Delay Vectors
        \item Loss Vectors
        \item Gain Vectors
        \item Transition Vectors
        \item Indicator Vector
        \item Net Gain Vector
    \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}[shrink=10]
\frametitle{Related Work}
\begin{alertblock}{Equations for computing for SNPs}
    For SNPs \textit{without} delay: 
    $$ C_{k+1} = C_k + S_k \cdot M_\Pi $$ 
    For SNPs \textit{with} delay:
    $$ C_{k+1} = C_k + NG_k $$
    Where $NG_k$ is a product of operations involving all the vectors before it
\end{alertblock}
\pause
    \textbf{The differences in the algorithms for the two are considerably different!}
\end{frame}

\begin{frame}
\frametitle{Related Work}
\centering
\begin{itemize}
    \item<1-> Spiking Neural P systems with Division and Budding (SNPDB systems) 
        are SNP systems with neurons that can divide or bud new neurons.
    \item<2-> Computationally faster for NP-hard problems.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Related Work}
\begin{itemize}
    \item Spiking Neural P systems with Division and Dissolution (SNPDD systems) 
        are SNP systems with neurons that can divide or dissolve.
\end{itemize}
\end{frame}

\begin{frame}[shrink=10]
\frametitle{Theoretical Framework}
\begin{alertblock}{Definition: Spiking Neural P Systems with Division and Dissolution} 
An SNPDD system of a finite degree $m \leq 1$ is a construct of the form\\
    $\Pi = (O, H, syn, n_1, n_2,...,n_m,R,in,out)$, where:
    \begin{enumerate}
        \item
            $O = \{s\}$ is the \textit{singleton alphabet} where $s$ is a spike.
        \item
            $H$ is the set of \textit{labels} for the neurons.
        \item
            $syn \subseteq H \times H$ represents a \textit{dictionary of the synapses} for each
            and every neuron ($1 \leq i \leq m, (i,i) \notin syn$)
        \item
            $n_i \geq 0$ represents the \textit{initial number of spikes} in neuron $\sigma_i$ for
            $1 \leq i \leq m$.
        \item
            $R$ represents the set of \textit{all developmental rules}, taking either of the four forms:
            \begin{itemize}
                \item Firing rule $[E/a^c\rightarrow a^P;d]$
                \item Forgetting rule $[E/a^c \rightarrow \lambda]$
                \item Neuron Division rule $[E]_i \rightarrow []_j || []_k$
                \item Neuron Dissolution rule $[E]_i \rightarrow \sigma_i$
            \end{itemize}
        \item
            $in, out \subseteq H$ indicate the \textit{input and output} neurons of $\Pi$, respectively.
    \end{enumerate}

\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Related Work}
\begin{itemize}
    \item
        Spiking Neural P systems with Division and Dissolution (SNPDD systems) 
        are SNP systems with neurons that can divide or dissolve.
    \item
        Faster than SNPDBs.
\end{itemize}
\end{frame}

\begin{frame}
    \centering
    \frametitle{Related Work}
    \begin{alertblock}{Definition: SAT Problem}
        $\phi = (\overline{x} \land y) \lor (x \land \overline{z})  $ \\
        $SAT = \{\langle \phi \rangle | \phi $ is a satisfiable Boolean formula$ \} $
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Simulation of SAT}
    \centering
    \includegraphics[scale=0.3]{sat_1.png}
\end{frame}

\begin{frame}
    \frametitle{Simulation of SAT}
    \centering
    \includegraphics[scale=0.3]{sat_2.png}
\end{frame}

\begin{frame}
    \frametitle{Simulation of SAT}
    \centering
    \includegraphics[scale=0.3]{sat_3.png}
\end{frame}

\begin{frame}
    \frametitle{Simulation of SAT}
    \centering
    \includegraphics[scale=0.3]{sat_4.png}
\end{frame}

\begin{frame}
    \frametitle{Simulation of SAT}
    \centering
    \includegraphics[scale=0.3]{sat_5.png}
\end{frame}

\begin{frame}
    \frametitle{Related Work}
    \begin{enumerate}
        \item<1->
            NVIDIA's Compute Unified Device Architecture (CUDA) allows
            scientific work on their GPUs.
        \item<2->
            Work can be loaded into the GPU hardware better and faster than CPU solutions.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Related Work}
    \begin{alertblock}{Required Global Memory for an SNP system}
        $$ G \geq q_k (n + 2m) + m (n + 1) $$ 
        where $q_k$ is the number of configuration vectors, $m$ is the number of neurons and $n$
        is the number of rules.
    \end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Related Work}
    \begin{enumerate}
        \item
            NVIDIA's Compute Unified Device Architecture (CUDA) allows
            scientific work on their GPUs.
        \item
            Work can be loaded into the GPU hardware better and faster than CPU solutions.
    \end{enumerate}
\end{frame}

\section{Statement of the Problem}
\begin{frame}
\frametitle{Statement of the Problem}
\begin{enumerate}
    \item<1->
        Simulation by pen-and-paper takes time.
    \item<2->
        SNPDDs need to be parametrized to form an algorithm.
    \item<3->
        CUDA can provide a powerful framework for the simulator.
\end{enumerate}
\end{frame}

\section{Significance of the Study}
\begin{frame}
\frametitle{Significance of the Study}
    \begin{enumerate}
        \item<1->
            SNPDDs are capable of solving NP-hard problems in linear time.
        \item<2->
            A simulator can significantly lessen time spent on computing.
        \item<3->
            Provide a basis for future simulators.
    \end{enumerate}
\end{frame}

\section{Objectives of the Study}
\begin{frame}
    \frametitle{Objectives of the Study}
    \begin{enumerate}
        \item<1->
            Parametrize the SNPDD system in such a way that it may be represented in terms of vectors and matrices, 
            computable through mathematical operations.
        \item<2->
            Create a simulator for SNPDD systems through NVIDIA's CUDA technology.
        \item<3->
            Expand upon existing simulators.
    \end{enumerate}
\end{frame}

\section{Scope and Limitations of the Study}
\begin{frame}
    \frametitle{Scope and Limitations of the Study}
    \begin{enumerate}
        \item<1->
            The algorithm must work on any SNPDD as input.
        \item<2->
            Other forms of the SNP system will not be considered.
    \end{enumerate}
\end{frame}

\section{Research Plan}
\begin{frame}
    \frametitle{Research Plan}
    \centering
    \includegraphics[scale=0.25]{gantt.png}
    \begin{enumerate}
        \item
            Phase 1: Research
        \item
            Phase 2: Formulation of algorithm
        \item
            Phase 3: Programming of simulator
        \item
            Phase 4: Writing of paper and benchmarking
    \end{enumerate}
\end{frame}

\section{Bibliography}
\frametitle{Bibliography}
\begin{frame}[shrink=30]
\nocite{*}
\bibliography{refs}
\bibliographystyle{apalike}
\end{frame}

\end{document}

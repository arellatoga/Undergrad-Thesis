\documentclass[a4paper,12pt]{article}
\usepackage[table]{xcolor}
\usepackage{tabularx}
%\usepackage{graphics}
%\usepackage{epsfig}
\usepackage{times}
\usepackage{amsmath}
\usepackage{tikz}
\usetikzlibrary{calc}
%\usepackage{pgfgantt}

\newtheorem{mydef}{Definition}
\newtheorem{mythm}{Theorem}
\newtheorem{mylem}{Lemma}
\newtheorem{myobserve}{Observation}
%\usepackage{upgreek}

\usepackage{amsmath,amssymb,latexsym,graphicx,url,listings, algorithm, algorithmic}
\usepackage {ulem,textcomp}
\usepackage {todonotes}
\usepackage{hyperref,multirow}
\usepackage[english]{babel}
\usepackage[UKenglish]{isodate}% http://ctan.org/pkg/isodate, for DD MM YYY date format


\newcommand{\ra}{\rightarrow}
\newcommand{\la}{\leftarrow}
\newcommand{\lra}{\longrightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\Lra}{\Longrightarrow}
\newcommand{\Piu}{\rm\Pi}


\newcommand{\addnote}[1]{\color{blue} #1 \color{black}}
\newcommand{\remnote}[2]{\color{red} \sout{#1} \color{blue} #2 \color{black}}
\newcommand{\modnote}[1]{\color{red} #1 \color{black}}


\topmargin      0.0in
\headheight     0.0in
\headsep        0.0in
\oddsidemargin  0.0in
\evensidemargin 0.0in
\textheight     9.0in
\textwidth      6.5in

\title{{\bf There and back again!} \\
\it (Title for report template)}
\author{ {\bf Arel Latoga}  \\
Algorithms and Complexity lab \\
Department of Computer Science \\
University of the Philippines Diliman\\
{\small \href{mailto:ozone@mosphere.edu.ph}{ozone@mosphere.edu.ph}}
}

\date{\cleanlookdateon \today}

\begin{document}
\pagestyle{plain}
\pagenumbering{roman}
\maketitle
\thispagestyle{empty} %removes page number in title page

\pagebreak
\begin{abstract}

{\color{red} Summary, in one page, of the entire proposal}

Computationally hard problems exist in various areas of human importance, although their solutions
require vast amounts of time or space. The area of natural computing, which includes branches such as
DNA and quantum computing, can improve our solutions and implementations for hard problems.
These branches can provide efficient solutions to hard problems with some trade-offs, oftentimes
requiring different physical media without the limitations of our modern silicon-based computers.

A recent branch in natural computing is membrane computing, which seeks to abstract computing ideas
from the functioning of biological cells. Models in membrane computing are called P systems, which
are parallel, distributed, nondeterministic, and computationally universal models. P systems at the
moment cannot be fully implemented in our current computers, although certain aspects of P systems
have been implemented in existing parallel hardware and software for simulation and verification. The
focus of this research will be on Spiking Neural P systems (SNP systems for short). In SNP systems,
neurons are nodes in a directed graph, where edges between neurons are synapses. Neurons process
spikes and send these to other neurons via synapses. Spikes are indistinct signals and do not encode
information. Instead, information can be derived for example by taking the time difference between
two successive spikes.

In this research we introduce and investigate SNP systems with structural plasticity features from
neuroscience: their computing power and limitations, relations to other computing models, and
solutions to hard problems. The investigations will emphasize other possible practical computing applications of
such neural features, as well as improve existing simulators of SNP systems on parallel software and
hardware.

\end{abstract}

\pagebreak
\tableofcontents %how to include references in Table of contents?
\pagebreak

\cleardoublepage
\pagenumbering{arabic}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background of the Study}
\label{ch:intro}

{\color{red} This part provides an overall introduction of your work, including the circumstances (whether practical or theoretical) that led to the inception of the work or problem(s) to be solved.}

The two aspects of computing, theory and application, have their own fundamental limits: for the former we have computationally hard problems (or simply, hard problems, if there is no confusion) that require vast amounts of time or space in their solutions; for the latter we have implementations, technologies, and physical limitations of the media used. Solving these hard problems translates to solving many of our real world problems in mathematics, physics, economics, biology, and more. For example, the well known \textit{Hamiltonian cycle problem}, also known as the \textit{travelling salesman problem}, asks if a given network or graph of $n$ nodes has a cycle that visits all the other nodes only once. We know that the solution to the traveling salesman problem is computationally hard, and being able to efficiently solve such a problem entails important ramifications in other fields of study. Improving or replacing our current silicon-based computers with better materials could allow faster execution of programs with increased efficiency, e.g. less consumed time, space or energy. 


\section{Theoretical Framework}
\label{ch:theor-frame}

{\color{red} This section includes the concepts, techniques etc. to be used for the research.}

\begin{figure}[htb]
\centering
\unitlength=1mm
\special{em:linewidth 0.4pt}
\linethickness{0.4pt}
\begin{picture}(116.33,40)(1,0)

%SYNTAX: \put(x,y)
%ovals i.e. membranes: \oval(x,y)
\put(20.83,20.17){\oval(50.00,45.67)[]} %region 1

\put(32.83,19.17){\oval(22.00,40.67)[]} %region 2

\put(7.83,10){\oval(20.00,20.67)[]} %region 3

\put(32.83,33){\oval(20.00,8.67)[]} %region 4

\put(32.83,6){\oval(20.00,8.67)[]} %region 5

%contents of regions
%region_1
\put(-5,2){\makebox(0,0)[cc]{$1$}}
\put(15.33,38.67){\makebox(0,0)[cc]{$ddc$}}
\put(10.3,31.67){\makebox(0,0)[cc]{$d \rightarrow (b,in_3)$}}
%\put(5.6,21.67){\makebox(0,0)[cc]{$d \rightarrow c$}}
\put(10.3,26.67){\makebox(0,0)[cc]{$c \rightarrow (d,in_2)$}}
%\put(49.8,6.67){\makebox(0,0)[cc]{$R_{c_{11}}: c \rightarrow b$}}
%\put(53.5,2.67){\makebox(0,0)[cc]{$R_{c_{12}}: c \rightarrow d^2b^2c^2$}}

%region 2
\put(22,1){\makebox(0,0)[cc]{$2$}}
%\put(32.00,10.67){\makebox(0,0)[cc]{$d$}}
\put(33.33,21.67){\makebox(0,0)[cc]{$d \rightarrow (d,in_4)$}}
\put(33.33,16.67){\makebox(0,0)[cc]{$d \rightarrow (d,in_5)$}}

%region 3
\put(17,1){\makebox(0,0)[cc]{$3$}}

%region 4
\put(24,27.5){\makebox(0,0)[cc]{$4$}}
\put(33.33,32.67){\makebox(0,0)[cc]{$d \rightarrow a$}}

%region 5
\put(23.5,11){\makebox(0,0)[cc]{$5$}}
\put(33.33,6){\makebox(0,0)[cc]{$d \rightarrow b$}}

%%%
%SNP
%%%
\put(65.83,31){\oval(21.00,10.67)[]} %neuron 1
\put(56,25.5){\makebox(0,0)[cc]{$1$}}
\put(65.33,33.67){\makebox(0,0)[cc]{$a^2$}}
\put(65.33,28.67){\makebox(0,0)[cc]{$a^+/a \rightarrow a$}}
\put(76,28.67){\vector(1,0){8}} %\sigma_1 -> \sigma_2
\put(67,25.67){\vector(1,-1){12}} %\sigma_1 -> \sigma_3

%
\put(95.83,31){\oval(23.00,17.67)[]} %neuron 2
\put(84,25.5){\makebox(0,0)[cc]{$2$}}
\put(95.33,36.67){\makebox(0,0)[cc]{$a^3$}}
\put(95.33,30.67){\makebox(0,0)[cc]{$(a^2)^+/a \rightarrow a$}}
\put(95.33,25.67){\makebox(0,0)[cc]{$a^2 \rightarrow \lambda$}}
\put(84.5,31.67){\vector(-1,0){8}} %2 -> 1
\put(89.5,22.3){\vector(-1,-1){9}} %2 -> 3

%
\put(80.83,8){\oval(21.00,10.67)[]} %neuron 3
\put(69,5){\makebox(0,0)[cc]{$3$}}
%\put(70.33,7.67){\makebox(0,0)[cc]{$a^2$}}
\put(80.33,8.67){\makebox(0,0)[cc]{$a^+/a \rightarrow a$}}
\put(80.5,3){\vector(0,-1){5}} %3 -> env
%\put(70.5,-3.5){\line(0,-1){5}} %
%\put(69,-1){\makebox(0,0)[cc]{$|$}}



\end{picture}
\caption{A cell-like P system (left) and an SNP system (right). }\label{fig:example}
\end{figure}


A common type of P system is the one shown in left of Figure \ref{fig:example}: it shows a compartmentalized \textit{cell-like} P system with five membranes (numbered 1 to 5); Membranes 2 and 3 are inside membrane 1 (the skin membrane), while membranes 4 and 5 are inside membrane 2; each membrane can have objects and rules inside. The objects or symbols (in the figure, membrane 1 has $d^2$ or two copies of $d$ and one of $c$) represent chemicals inside the cell and its membranes; each membrane can also have several rules which transform objects from one kind to another, and these rules represent chemical reactions inside the cell which transform chemicals from type to another; Examples of rules include the rule $d \rightarrow (d, in_4)$ in membrane 2 and the rule $c \rightarrow (d,in_2)$ in membrane 1. Membranes can apply rules in parallel, and can also communicate with each other in a distributed manner by sending or receiving objects. If more than one rule can be applied, the system can nondeterministically choose one rule to apply.



\begin{mydef}[SNP system]\label{snp-defn} A Spiking Neural P system (SNP system) of a {finite} degree $m \geq 1$  is a construct of the form ${\Piu}=(O,\sigma_1,\ldots, \sigma_m, syn, out),$%\todo{might consider removing $in$ from the definition just as \cite{snp-pnet} did}
%\end{definition}\todo{SNP system w/ delay?}
where: %\todo{$b$ should be either 1 or 0 only...}
\begin{enumerate}
\item[1.] $O=\{a\}$ is the singleton alphabet ($a$ is called \textit{spike}).

\item[2.] $\sigma_1,\ldots, \sigma_m$ are pairs $\sigma_{i}=(n_i, {\mathcal{R}_i}),1\leq i \leq m$, called neurons, where $n_i \geq 0$ and $ n_i \in \mathbb{N} \cup \{0\}$ represents the initial spikes in $\sigma_i$, ${\mathcal{R}_i}$ is a finite set of rules of the form $E/a^c \rightarrow a^b;d$ where $E$ is a regular expression over $O$, $c \geq 1$, if $b \geq 1$ then $d \geq 0$ and $c \geq b$, else if $b = 0$ then $d = 0$.
	
	\item[3.] $syn$, is the synapse set, a nonreflexive relation on $\{ 1, \ldots, m \} \times \{1, \ldots, m\}$ %$\subseteq \{1, 2, \ldots, m\} \times \{ 1, 2, \ldots, m \}$, $(i,i) \notin syn$ for $1 \leq i \leq m$, are synapses between neurons.

	\item[4.] $out\in \{1,2,\ldots, m\}$ is the index of the \textit{output} neuron.
\end{enumerate}
\end{mydef}

%An SNP system is a directed graph where nodes (often drawn as ovals) are multiset processors and are called \textit{neurons}. The arcs, known as \textit{synapses}, connect one neuron to another. 
A \textit{spiking rule} is where $b \geq 1$. A \textit{forgetting rule} is where $b = 0$ and is written as $E/a^c \rightarrow \lambda$. If $L(E) = \{a^c\}$ then spiking and forgetting rules are simply written as $a^c \rightarrow a^b$ and $a^c \rightarrow \lambda$, respectively. Applications of rules are as follows: if neuron $\sigma_i$ contains $k$ spikes, $a^k \in L(E)$ and $k \geq c$, then the rule $E/a^c \rightarrow a^b \in \mathcal{R}_i$ must be applied. If $b = 1$, the application of this rule consumes or removes $c$ spikes from $\sigma_i$, so that only $k - c$ spikes remain in $\sigma_i$. The neuron sends a spike to every $\sigma_j$ such that $(i,j) \in syn$. The output neuron has a synapse not directed to any other neuron, only to the environment. The neuron $\sigma_1$ (or the first in the defined order) is referred to as the \textit{initial neuron}.

A delay is a $d \in \mathbb{N} \cup \{0\}$. If a spiking rule has delay $d = 0$ (forgetting rules do not have delays) then $d$ is omitted from writing, and a spike is sent immediately i.e. in the same time step as the application of the rule. If $d \geq 1$ and the spiking rule was applied at time $t$, then the spike is sent at time $t+d$. From time $t$ to $t+d-1$ the neuron is said to be \textit{closed} {(inspired by the \textit{refractory period} of the neuron in biology)} and cannot receive spikes. Any spikes sent to the neuron when the neuron is closed are \textit{lost} or removed from the system. At time $t+d$ the neuron becomes \textit{open} and can then receive spikes again. The neuron can then apply another rule at time $t+d+1$.  

For a forgetting rule, no spikes are produced (they are forgotten or removed from the system). SNP systems assume a global clock, so the application of rules and the sending of spikes by neurons are all synchronized. 
The \textit{nondeterminism} in SNP systems occurs when, given two rules $E_1/a^{c_1} \rightarrow a^{b_1}$ and $E_2/a^{c_2} \rightarrow a^{b_2}$, it is possible to have $L(E_1) \cap L(E_2) \neq \emptyset$. In this situation, only one rule will be nondeterministically chosen and applied. SNP systems are \textit{globally parallel} (neurons operate in parallel) but are \textit{locally sequential} (at most one rule per neuron is used). %In this work however we will only be considering \textit{deterministic} SNP systems.
Note that if a spiking rule can be applied, then there is no forgetting rule that can be applied, and vice versa i.e. if a spiking and forgetting rule have regular expressions $E_{spik}$ and $E_{forg}$ respectively in a neuron, then $L(E_{spik}) \cap L(E_{forg}) = \emptyset $.

A configuration of the system at time $k$ is denoted as $C_k = \langle r_1/t_1$, $\ldots$, $r_m/t_m, r_e \rangle$, where each element of the vector (except for $r_e$, representing the spikes in the environment) represents the number of the symbol $a$ (spikes) in neuron $\sigma_i$, with $r_i \geq 0, r_i \in \mathbb{N} \cup \{0\} $ and neuron $i$ is open after $t_i \geq 0$ steps. When a rule in a neuron with delay $d = t_i$ is fired, $t_i$ is decreased by one every time step until the neuron fires when $t_i = 0$. An initial configuration $C_0$ is therefore $ \langle n_1/0, \ldots, n_m/0, 0 \rangle $ since no rules whether with or without delay, have yet been applied and the environment is initially empty. %$t_i$ increases when $\sigma_i$ applies a rule with delay.
A \textit{computation} is a sequence of transitions from an initial configuration. A computation may halt (no more rules can be applied for a given configuration) or not. If an SNP system does halt, all neurons should be open. The result of a computation can be interepreted in several ways, and a common way is to take the time difference between exactly the two spiking times of the output neuron.
%Computation result in this work is obtained by checking the number of spikes in the environment once the system halts. %{A \textit{path} exists between $\sigma_i$ and $ \sigma_m$ if there exist a series of neurons $\sigma_{j}, \sigma_k, \ldots, \sigma_l$ and a series of synapses $(i,j), (j,k), \ldots, (l,m)$ between them, leading from $ \sigma_i$ to $\sigma_m$.} 





\section{Related work}
\label{ch:related}

{\color{red} In this section, existing work must be compared and contrasted with the proposed work.}

Sample citations: \cite{ionescu-sndp-2011}, \cite{Paun:WUMC:Introduction_MC:2002}



\section{Problem Statement}\label{ch:prob-sate}

{\color{red} Must state the question(s) that will be answered by the proposed research. }

One main idea of works in natural and membrane computing is obtaining inspiration from natural processes for computing use. This research continues this idea on how structural plasticity can be used as inspiration for membrane and neural-like computing. In particular, we want to ask the following questions:
\begin{itemize}
	\item How do we introduce and use structural plasticity features in SNP systems theory? 

	\item How computationally powerful are {SNP systems with structural plasticity (SPSNP systems)}? What are their limitations?
	\item How do SPSNP systems compare or relate to other P systems and other non-P system models, e.g. Petri nets, process algebra?
	\item How can we use SPSNP systems to solve computationally hard problems?
\end{itemize}



\section{Significance of the study}
%\todo{Add implications of study.}

{\color{red} What is the impact (e.g. on theory work, practical work) of the proposed research once questions from problem statement are answered?}

The significance of this research is split into two parts, theory and practice, with direct and indirect signifiance for both parts.

\textit{Theory}: %SPSNP systems can provide the possibility of solving hard problems efficiently, given some trade-offs.
The direct theoretical significance will be on the introduction of SPSNP systems, their power, limitations, techniques to solve hard problems, and how they compare and relate to other computing models. %The idea of using structural plasticity features from neuroscience in the field of membrane computing is a novel idea 
Indirect theoretical significance includes the possible use of SNP and SPSNP systems in modeling, analysis, and verification of systems, e.g. network survivability, mobile ad hoc networks, since neurons are essentially interconnected spike processors and structural plasticity features could be used to model the creation or loss of connections between networked computers and other mobile agents under certain conditions; certain aspects of stroke, and Alzheimer's disease, adding tools useful to neuroscience.

\textit{Practice}:
The direct practical significance includes techniques how to simulate SPSNP systems and improve existing simulators of SNP systems in GPUs using open source software in the implementations. Such parallel simulators vastly improve the execution times of experiments, aside from aiding verification and analysis. The open source implementations allow other researchers to futher improve the created simulators.  Indirectly, the research will highlight limitations of existing software and GPUs which can be used to improve their next generations. GPUs, even with their massive parallelism at the moment, cannot faithfully implement SNP systems in general. The implementations during the improvement of the simulators will reveal specific strengths and weaknesses of GPUs in simulating similar systems, which are relevant to other science and engineering disciplines.

\section{Objectives of the Study}\label{object-sect}

{\color{red} specific objectives of the study to achieve or answer the questions in problem statement.}

The main objective of this research is to investigate SNP systems with structural plasticity features, i.e. SPSNP systems. In particular:  %in relation to the computing power and complexity of other computing models. Structural plasticity in the context of SNP systems is also to be explored for its computing use. The specific objectives of this research are as follows:

\begin{enumerate}

\item Use structural plasticity features in SNP systems computations, e.g. in solving hard problems.

\item Relate SPSNP systems and other SNP system variants to other P systems and non-P systems models, e.g. Petri nets, process algebra.

%\item Introduce SPSNP systems, an SNP system variant, and realize the computing power and limitations of such a variant.

\item Improve existing simulators of SNP systems on massively parallel hardware, i.e. graphics processing units (GPUs), to better aid our understanding and experiments. % Identify techniques and 


\end{enumerate}

\section{Scope and Limitations of the Study}

{\color{red} What is the scope of the proposed research? Which factors, variables, concepts, conditions etc. are not included in this scope?}






%\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%



%\pagebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Research plan} %this is the methodology and timetable!
\label{ch:plan}

{\color{red} Provide an overview of what you have done (so far, if any) and what needs to be done.}
%Given the proposed topics in section \ref{ch:proposal}, the research timetable in order to complete the topics is given in this section.}

\subsection{Methodology}\label{ch:method}

In order to complete the objectives in section \ref{object-sect}, the following are the research methodology phases.

\begin{enumerate}
\item[1:] Further literature review of related work on the proposed topics, including recent results from conferences and journals; Further study computability theory and computational complexity of deterministic and nondeterministic models, and different types of behavioural equivalences and simulations in process algebra and Petri nets; Also, investigate other neural phenomena useful in SNP systems, SPSNP systems, and their variants. 

\item[2:] Introduce structural plasticity features in SNP systems theory: Study SPSNP systems by proving their computational universality, what such systems can compute given certain restrictions or constraints %, the techniques and encodings needed in order to solve hard problems; SPSNP systems will also be related to other P systems and non-membrane system models with respect to their computing power, limitations, complexity.


\item[3:] Study the computing power, limitations, complexity, and techniques to solve hard problems using SPSNP systems

\item[4:] Study and relate SNP and SPSNP systems with other P system and non-P system models e.g. Petri nets, process algebra; %Study and relate SNP systems to Petri nets, including their variants, and how they can simulate each other; Realize other possible further applications of SNP systems as well as Petrnets. This phase accomplishes objective 2.

%This phase accomplishes objectives 3 and 4.


\item[5:] Use open-source software to improve existing SNP systems simulators running on GPUs


\item[6:] Document results from phase 1 to 5; Publish results in refereed conferences and journals (local and international)

\end{enumerate}


\subsection{Timetable}
Figure \ref{gantt-fig} shows the Gantt chart for this research, which includes phases 1 to 6 across the 21 month research timetable. The 21 months will include the planned international study (i.e. sandwich program) to be funded by {the DOST Engineering Research and Development for Technology (ERDT for short) scholarship} starting February 2014. 

\subsection{Research outputs}

After the 21 month research period (inclusive of the 12 months in the RGNC) mentioned in Figure \ref{gantt-fig}, the research outputs will include the following:

\begin{itemize}
	\item Refereed conference papers and journals articles (local and international) and dissertation manuscript on:
	\begin{itemize}
		\item Introduction of structural plasticity features in SNP systems theory for computing use, and in particular:
			\begin{itemize}
				\item Computing power, limitations, comparisons and relations to other computing models, techniques to solve hard problems
				\item Identification of further possible practical applications, e.g. modeling, analysis, verification of systems.
				\item Techniques how to implement and improve SNP systems simulations on parallel hardware, i.e. GPUs
			\end{itemize}
		\item Improved GPU simulator software for SNP systems using open source software
	\end{itemize}
\end{itemize}


% GanttHeader setups some parameters for the rest of the diagram
% #1 Width of the diagram
% #2 Width of the space reserved for task numbers
% #3 Width of the space reserved for task names
% #4 Number of months in the diagram
% In addition to these parameters, the layout of the diagram is influenced
% by keys defined below, such as y, which changes the vertical scale
\def\GanttHeader#1#2#3#4{%
 \pgfmathparse{(#1-#2-#3)/#4}
 \tikzset{y=7mm, task number/.style={left, font=\bfseries},
     task description/.style={text width=#3,  right, draw=none,
           font=\sffamily, xshift=#2,
           minimum height=2em},
     gantt bar/.style={draw=black, fill=blue!30},
     help lines/.style={draw=black!30, dashed},
     x=\pgfmathresult pt
     }
  \def\totalmonths{#4}
  \node (Header) [task description] at (0,0) {\textbf{Phases}};
  \begin{scope}[shift=($(Header.south east)$)]
    \foreach \x in {1,...,#4}
      \node[above] at (\x,0) {\footnotesize\x};
 \end{scope}
}

% This macro adds a task to the diagram
% #1 Number of the task
% #2 Task's name
% #3 Starting date of the task (month's number, can be non-integer)
% #4 Task's duration in months (can be non-integer)
\def\Task#1#2#3#4{%
\node[task number] at ($(Header.west) + (0, -#1)$) {#1};
\node[task description] at (0,-#1) {#2};
\begin{scope}[shift=($(Header.south east)$)]
  \draw (0,-#1) rectangle +(\totalmonths, 1);
  \foreach \x in {1,...,\totalmonths}
    \draw[help lines] (\x,-#1) -- +(0,1);
  \filldraw[gantt bar] ($(#3, -#1+0.2)$) rectangle +(#4,0.6);
\end{scope}
}

% Example
\begin{figure}
\thispagestyle{empty}
\begin{tikzpicture}
  \GanttHeader{.8\textwidth}{2ex}{2cm}{20}
  \Task{1}{$ \sim $8 mos.}{0}{8.5}
  \Task{2}{$ \sim $6 mos.}{0.5}{6.5}
  \Task{3}{$ \sim $8 mos.}{3.5}{8.5}
  \Task{4}{$ \sim $12 mos.}{6.5}{12.5}
  \Task{5}{$ \sim $5 mos.}{14}{5}
  \Task{6}{$ \sim$20 mos.}{1.5}{18.5}
%  \Task{7}{Prepare final report}{11}{1}
\end{tikzpicture}
\caption{Month 1 is September 2013, month 20 is April 2015. Dissertation defense is May 2015 (21 month total)} 
\label{gantt-fig}
\end{figure}

%\pagebreak
\subsection{A sample table}



\begin{table}[H]
\centering
  \rowcolors{2}{gray!25}{white}
  \begin{tabular}{cc}
    \rowcolor{gray!50}
    Description(s) & Cost in PHP\\
    Local (PH) transportation to and from Terminal 1 & 1,000.00\\
    insurance & 00.00\\
    Relocation costs  & 00.00 \\  
    TOTAL & 00.00
  \end{tabular}
  \caption{Sample table}
  \label{exp-tab}
\end{table}


%\pagebreak


\bibliographystyle{apalike}
\bibliography{refs}

\end{document}

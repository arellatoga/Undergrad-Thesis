#pragma once
#include "neuron.hpp"
#include <vector>

class rule {
    public:
    rule(int _type) {
        type = _type;
    }
    
    private:
    int type;
};

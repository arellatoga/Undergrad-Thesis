#include <iostream>
#include <string>

/* Forms:
 * a^*, x = 0, y = 1
 * a^+, x = 1, y = 1
 * a^k, x = k, y = - 1
 * a^k(a^j)^*, x = k, y = j
 * a^k(a^j)^+, x = k + j, y = j
 */

enum REGEX_FORM {
    STAR = 0, 
    PLUS,
    FIXED,
    FIXED_STAR,
    FIXED_PLUS
};

class regex_parser {
    public:
    // returns the "consumed" amount of spikes
    int solve(int spike_count, int form, int j, int k) {
        if (form == FIXED) {
            return k;
        }
        else if (form == STAR) {
            return 0 % 1;
        }
        else if (form == PLUS) {
            return 1 % 1;
        }
        else if (form == FIXED_STAR) {
            return k % j;
        }
        else if (form == FIXED_PLUS) {
            return (k + j) % j;
        }
        else {
            std::cout << "Invalid form requested!" << std::endl;
        }
    }

    private:
};

    

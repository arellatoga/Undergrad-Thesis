#include "rule.hpp"

Rule::Rule(string _neuron_name, int _type) {
    neuron_name = _neuron_name;
    type = _type;
}

// Fire rule
Rule::Rule(string _neuron_name, int _type, int _spike) {
    neuron_name = _neuron_name;
    type = _type;
    spike = _spike;
}

// Divide rule
Rule::Rule(string _neuron_name, int _type, string _child_a, string _child_b) {
    neuron_name = _neuron_name;
    type = _type;
    child_a = _child_a;
    child_b = _child_b;
}
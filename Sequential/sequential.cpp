#include <iostream>
#include <string>
#include <vector>
#include <utility>

// Boost libraries
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>

// personal libraries
#include "rule.hpp"
#include "neuron.hpp"
#include "synapse.hpp"
#include "regex.cpp"

// To save a few keystrokes
using namespace std;
using namespace boost::numeric::ublas;

// All the matrices involved
matrix<int> configVector;
matrix<int> netGainVector;

// Gain Vector and its components
matrix<int> gainVector;
matrix<int> indicatorVector;
matrix<int> transitionVector;

// Loss Vector and its components
matrix<int> lossVector;
matrix<int> spikingVector;
matrix<int> ruleMatrix;

// Other vectors
matrix<int> statusVector;
matrix<int> maskVector;
matrix<int> trainVector;
matrix<int> divisionVector;
matrix<int> ruleRepresentation;
matrix<int> ruleNeuronCorrespondence;

// function prototypes
void simulate_snp();
matrix<int> matrix_elementwise_mult(matrix<int>, matrix<int>);
matrix<int> reset(matrix<int>);
matrix<int> compute_spiking_vector();

// regex parser
regex_parser regexParser;

enum ruleType {
    FIRE = 0,
    FORGET,
    DIVIDE,
    DISSOLVE
};

int main () {
    maskVector = matrix<int> (1, 5);
    spikingVector = matrix<int> (1, 5);

    maskVector (0, 0) = 0;
    maskVector (0, 1) = 1;
    maskVector (0, 2) = 0;
    maskVector (0, 3) = 1;
    maskVector (0, 4) = 0;

    spikingVector (0, 0) = 0;
    spikingVector (0, 1) = 1;
    spikingVector (0, 2) = 2;
    spikingVector (0, 3) = 3;
    spikingVector (0, 4) = 4;

    simulate_snp();
    cout << "Program terminated" << endl;
}

void simulate_snp() {
    matrix<int> oldMaskVector = maskVector;
    // Get the rules which are supposed to be division or dissolution
    matrix<int> checkVector = matrix_elementwise_mult(spikingVector, maskVector);
    cout << "check vec done" << endl;
    
    for (int i = 0; i < checkVector.size2(); ++i) {
        if (checkVector (0, i) == 1) {
            // divide
            //checkVector (0, i) = 0;
        }
        else if (checkVector (0, i) == -1) {
            // Dissolve 
        }
    }

    maskVector = oldMaskVector;
    reset(lossVector);
    reset(gainVector);
    reset(netGainVector);
    reset(indicatorVector);
    //compute_spiking_vector();
    //
}

matrix<int> reset(matrix<int> A) { 
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            A (i, j) = 0;
        }
    }

    return A;
}

matrix<int> matrix_elementwise_mult(matrix<int> A, matrix<int> B) {
    if (A.size1() != B.size1()) {
        std::cout << "Invalid row count!" << endl;
        return A;
    }
    else if (A.size2() != B.size2()) {
        std::cout << "Invalid column count!" << endl;
        return A;
    }

    matrix<int> returnMatrix (A.size1(), B.size2());

    cout << "Size: " << A.size1() << " " << B.size2() << endl;

    // pre-increments for ~code of good taste~
    for (int i = 0; i < A.size1(); ++i) {
        for (int j = 0; j < A.size2(); ++j) {
            returnMatrix(i, j) = A(i, j) * B(i, j);
            cout << returnMatrix(i, j) << endl;
        }
    }

    return returnMatrix;
}

matrix<int> compute_spiking_vector() {
    matrix <int> newSpikingVector (0, ruleRepresentation.size1());
    for (int i = 0; i < ruleRepresentation.size1(); i++) {
        // int l = neuron that contains the ith rule mentioned above.
        int l = ruleNeuronCorrespondence(0, i);

        if (statusVector(0, i) == 0) {
            newSpikingVector(0, l) = 0;
        }
        else {
            if (true /* parser thing */) {
                newSpikingVector(0, 1) = 1;   
            }
            else {
                newSpikingVector(0, 1) = 1;
            }
        }
    }
}

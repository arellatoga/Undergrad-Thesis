#pragma once
#include <string>

using namespace std;

class Rule {
    public:
    Rule(string, int);
    Rule(string, int, int);
    Rule(string, int, string, string);

    int type;
    int spike;
    string neuron_name;
    string child_a;
    string child_b;
};
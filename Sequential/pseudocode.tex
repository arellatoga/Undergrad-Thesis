\documentclass[11pt,a4paper]{article}

\usepackage[a4paper,margin=0.75in]{geometry}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsthm}
\usepackage{mathtools}
\setlength\parindent{0pt}

\newtheoremstyle{break}
{\topsep}{\topsep}
{}{}
{\bfseries}{:}
{\newline}{}
\theoremstyle{break}
\newtheorem{defn}{Definition}[section]

\begin{document}
gahl\\
2013-14806
\begin{defn}[\textbf{Mask Vector}]
    Given $n$ rules present on the system at the $kth$ step,
    a \textbf{Mask Vector} $MV^{(k)}$ is defined as 
    $MV^{(k)} = \langle mv_1, mv_2, ..., mv_n \rangle$ 
    where 
    \[
        mv^{k}_j =     
    \begin{cases}
        -1 & \text{if the rule is a dissolution rule} \\
        0 & \text{if the rule is either a spiking or forgetting rule} \\ 
        1 & \text{if the rule is a division rule}
    \end{cases}
    \]
    As the system modifies through division and dissolution, the length $n$ of the vector
    will grow or shrink with it.
\end{defn}

\begin{defn}[\textbf{Spike Train Vector}]
    A spike train can be defined as the construct:
    $$ST_i = (E, \sigma_i, a)$$
    \begin{enumerate}
        \item $E$ is the regular expression of the spike train
        \item $\sigma$ is the neuron it is attached to, identified by its index $i$ and
        \item $a$ is the number of spikes it sends
    \end{enumerate}
    At each step $k$, if $\alpha^k \in L(E)$ is satisfied, $a$ spikes are sent to $\sigma$.
    This is all assuming that spike trains are connected only to neurons already on the system
    from the start, and that the neuron it is connected to does not divide, or dissolve.\\
    The \textbf{Spike Train Vector} $STV^{(k)}$ is a vector which size is the same as the
    number of neurons present in the system $m$ and is defined as
    $SMV^{(k)} = \langle smv_1, smv_2, ..., smv_m \rangle$ where 
    \[
    smv_i =
    \begin{cases}
        a_i & \text{if $E_i$ of $ST_i$ is satisfied by the current $k$} \\
        0 & \text{otherwise}
    \end{cases}
    \]
\end{defn}

\begin{algorithm}
    \caption{Modified Algorithm for SNPDDs}
    \begin{algorithmic}[1]
        \Procedure{Simulate SNPDD}{}\Comment{Modified algorithm for SNPDDs}
        \State $Reset(LV^k)$
        \State $Reset(GV^k)$
        \State $Reset(NG^k)$
        \State $Reset(IV^k)$
        \State $Compute (S^k)$
        \State $CV \leftarrow S^k \otimes MV$ \Comment{Elementwise multiplication of $S^k$ and $MV$}
        \For{$cv \in CV$} \Comment {For a DD operation after the first, $j$ will have a compensated value}
            \If {$cv_j = 1$} \Comment {Check if the rule is a division rule}
                \State $S^k_i \leftarrow 0$ \Comment{Then set the spiking vector value to 0}
                \State $DivideNeuron(j)$
            \ElsIf {$cv_j = 0$} \Comment {Check if the rule is a dissolution rule}
                \State $DissolveNeuron(j)$
            \EndIf
        \EndFor
        \For{$r_i = \{E, j, d', c\} \in R$} \Comment{Perform the rest of the algorithm}
            \If {$S^k_i = 1$}
                \State $LV^k_j \leftarrow c$
                \State $d' \leftarrow d_i$
                \State $IV^k_i \leftarrow 0$
                \If{$d' = 0$}
                    \State $IV^k_i \leftarrow 1$
                    \State $St^k_i \leftarrow 1$
                \EndIf
            \ElsIf{$d' = 0$}
                \State $IV^k_i \leftarrow 1$
                \State $St^k_i \leftarrow 1$
            \EndIf
        \EndFor
        \State $GV^k \leftarrow TV * IV^k$
        \State $NG^k \leftarrow GV^k \otimes St^k - LV^k$
        \State $C^{k+1} \leftarrow C^k + NG^k$
        \For{$r_i = \{E, j, d', c\} \in R$}
            \If {$d' \neq -1$}
                \State $d' \leftarrow d' - 1$
            \EndIf
        \EndFor
        \State \Return $C^{k+1}$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

\end{document}

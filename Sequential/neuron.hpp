#pragma once
#include <string>
#include <vector>

using namespace std;

class Rule;

class Neuron {
    public:
    Neuron(string);
    vector<Rule*> rules;
    string name; 
    int spike;

    private:
};